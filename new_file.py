from hl7apy.core import *
import json


import json
f = open('example_data.json')
data = json.load(f)
f.close()


for (j, k) in data.items():

    if j == 'obr':
        obr = Segment('OBR')
        counter = 1

        for k, value in k.items():
            # print(value)

            variable = 'OBR' + '_' + str(counter)
            # print(variable)
            # print(value)
            f = Field(variable)
            f.value = str(value)
            obr.add(f)
            print(obr.to_er7())
            counter += 1

    if j == 'nte':
        nte = Segment('NTE')
    if j == 'obx':
        obx = Segment('OBX')
    if j == 'msh':
        msh = Segment('MSH')
    if j == 'sft':
        sft = Segment('SFT')
    if j == 'pid':
        pid = Segment('PID')

    # print(j)
