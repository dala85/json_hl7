from hl7apy.core import *
import json
import re


f = open('example_data.json')
data = json.load(f)
f.close()

message_header = 'MSH|^~\&|'
message_list = []
message_list.insert(0, message_header)
list_position = {
    'MSH': ['name', 'address,city,state,zip'.split(','), 'date'],
    'SFT': ['id,name'.split(','), 'version'],
    'PID': ['id', 'test,patient'.split(','), 'address,city,state,zip'.split(','), 'gender', 'ethnicity'],
    'NTE': ['note'],
    'OBR': ['order_no', 'ordering_phys_id,ordering_phys_name'.split(','), 'panel'],
    # 'OBX': ['name', 'address,city,state,zip'.split(','), 'date'],
}

list_lengths = {'MSH': 9, 'SFT': 4, 'PID': 6, 'NTE': 2, 'OBR': 7}
message_lines = []
for (segment_name, segment_dict) in data.items():

    # if segment_name.upper() == "MSH":
    # {field = name , value = Test Labortary }
    segment_name = segment_name.upper()
    message_list = [segment_name, '1']
    if segment_name in list_lengths:

        message_list.extend([''] * list_lengths[segment_name.upper()])

        for (field, value) in segment_dict.items():

            if field == 'name':
                message_list.insert(1, value)
            if field == 'address':
                message_list.insert(2, ('||||' + value))
            if field == 'city':
                message_list.insert(3, ('^' + value))
            if field == 'state':
                message_list.insert(4, ('^' + value))
            if field == 'zip':
                message_list.insert(5, ('^' + value))
            if field == 'date':
                date_time = ''.join([n for n in value if n.isdigit()])
                message_list.insert(6, ('|||' + date_time))

    else:
        print('Unkown segment_name : {}'.format(segment_name))

    message = ''.join(map(str, message_list))
    message_lines.append(message)

print('\n'.join(message_lines))
