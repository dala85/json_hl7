dictino = {"msh": {
    "name": "Test Laboratory",
    "address": "123 Test St",
    "city": "Test City",
    "state": "CA",
    "zip": "92121",
    "date": "2020-05-01 20:30:12"

}}

message = " MSH|^~\&|Test Laboratory||||123 Test St^Test City^CA^92121|||20200501203012 "


from hl7apy.core import *
import json
import re


f = open('example_data.json')
data = json.load(f)
f.close()
message_list = 'MSH|^~\&|'

for (j, k) in data.items():
    segment = j  # {msh,sft,pid,nte,obr,obx}
    # print(segment)
    if segment.upper() == "MSH":
        # {field = name , value = Test Labortary }
        for (field, value) in k.items():
            if field == 'name':
                message_list.join(value)
            if field == 'address':
                message_list[1].append('||||' + value)
            if field == 'city':
                message_list[2].append('^' + value)
            if field == 'state':
                message_list[3].append('^' + value)
            if field == 'zip':
                message_list[4].append('^' + value)
            if field == 'date':
                date_time = ''.join([n for n in value if n.isdigit()])
                message_list[5].append('|||' + date_time)

message = ' '.join(map(str, message_list))

print(message)

